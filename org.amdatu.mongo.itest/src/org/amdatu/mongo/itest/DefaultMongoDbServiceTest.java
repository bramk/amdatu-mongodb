/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.mongo.itest;

import static org.amdatu.testing.configurator.TestConfigurator.*;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import org.amdatu.mongo.MongoDBService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleException;

import com.mongodb.BasicDBObject;
import com.mongodb.CommandResult;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.ReadPreference;
import com.mongodb.WriteConcern;

import static org.junit.Assert.*;

public class DefaultMongoDbServiceTest {
	
	private static final String COLLECTION = "demo";
	private DBCollection m_collection;
	private volatile MongoDBService m_mongoDbService;
	private volatile BundleContext m_bundlecontext;
	
	@Before
	public void setUp() throws Exception {
		
		configure(this)
		.add(createFactoryConfiguration("org.amdatu.mongo"))
        .add(createServiceDependency().setService(MongoDBService.class).setRequired(true))
        .apply();
		
        // Check if the configured collection is actually available
		m_collection = MongoTestUtil.getDBCollection(m_mongoDbService, COLLECTION);
	}
	
	@Test
	public void testDefaultMongoDbService() throws Exception {
		DB db = m_mongoDbService.getDB();
		assertNotNull(db);
		assertEquals("test", db.getName());
		assertEquals(WriteConcern.ACKNOWLEDGED, db.getWriteConcern());
		assertEquals(ReadPreference.primary(), db.getReadPreference());
	}
	
	@Test
	public void testCleanupConnections() throws InterruptedException, BundleException {
		if (!canRunTest()) {
			return;
		}
		int nrOfConnections = getNrOfConnections();
		
		for(int i = 0; i < 10; i++) {
			DB db = m_mongoDbService.getDB();
			assertEquals("test", db.getName());
			db.getCollection(COLLECTION).save(new BasicDBObject("name", i));
			 
			Bundle bundle = findBundle(); 
			bundle.stop();
			bundle.start();
			
			
			TimeUnit.MILLISECONDS.sleep(500);
		}
		
		assertEquals(nrOfConnections, getNrOfConnections());
	}

	private Bundle findBundle() {
		return Arrays.stream(m_bundlecontext.getBundles()).filter(b -> b.getSymbolicName().equals("org.amdatu.mongo")).findAny().get();
	}
	
	private int getNrOfConnections() {
		DB db = m_mongoDbService.getDB();
		CommandResult status = db.command("serverStatus");
		DBObject currentConnections = (DBObject)status.get("connections");
		return (Integer)currentConnections.get("current");
	}
	
    private boolean canRunTest() {
        return m_collection != null;
    }
	
    @After
    public void after() {
    	cleanUp(this);
    }
}
