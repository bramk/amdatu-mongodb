/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.mongo.itest;

import org.amdatu.mongo.MongoDBService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

import com.mongodb.DB;
import com.mongodb.ReadPreference;
import com.mongodb.WriteConcern;

import static org.amdatu.testing.configurator.TestConfigurator.*;

public class ConfiguredMongoDbServiceTest {
	
	private volatile MongoDBService m_mongoDbService;
	
	@Before
	public void setUp() throws Exception {
		configure(this)
		.add(createFactoryConfiguration("org.amdatu.mongo").set("dbname", "mydb1").set("readPreference", "secondaryPreferred").set("writeConcern", "ACKNOWLEDGED"))
        .add(createServiceDependency().setService(MongoDBService.class).setRequired(true))
        .apply();
	}
	
	@Test
	public void testConfiguredMongoDbService() throws Exception {
		DB db = m_mongoDbService.getDB();
		assertNotNull(db);
		assertEquals("mydb1", db.getName());
		assertEquals(ReadPreference.secondaryPreferred(), db.getReadPreference());
		assertEquals(WriteConcern.ACKNOWLEDGED, db.getWriteConcern());
	}
	
	@After
    public void after() {
    	cleanUp(this);
    }
}
